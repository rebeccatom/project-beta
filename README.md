# CarCar

Team:

- Nolan Michtavy - Sales microservice
- Rebecca Tom - Services microservice

## Design

Professional design with muted, non-vibrant colors, table-based information, sharp edges, sans-serif fonts.

## Service microservice

Explain your models and integration with the inventory
microservice, here.

The service microservice keeps track of service appointments. The models include a Technician model, AutomobileVO model (structures Automobile VO data which is polled from the inventory microservice and identified by the "vin"), and an Appointment model. The Appointment model has a many-to-one relationship with the Technician model (since each appointment can only can one technician but technicians can have many appointments).

## Sales microservice

Salesperson, customer, sale models. Salesperson will have first_name, last_name, and employee_id. customer will have first_name, last_name, address, and phone_number. Sale will have automobile, salesperson, customer, and price. The sales microservice will keep track of all of the automobile sales with salesperson, customer, and pull the vin for said automobile from the inventory to specify which car is being sold. The sale model will be completely comprised of foreign keys except for the price property.

There will also be a AutomobileVO that will have the vin pulled from the inventory microservice, that will correspond to one complete automobile.
