from django.contrib import admin
from .models import Sale, Salesperson, Customer

@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    pass

@admin.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    pass

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass
