from django.shortcuts import get_object_or_404
from .models import Salesperson, Sale, AutomobileVO, Customer
import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .encoders import SalespersonEncoder, CustomerEncoder, SaleEncoder


@require_http_methods(["GET", "POST"])
def api_list_or_create_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "PUT"])
def api_delete_or_update_salesperson(request, id):
    if request.method == "DELETE":
        salesperson = get_object_or_404(Salesperson, employee_id=id)
        count, _ = salesperson.delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Salesperson.objects.filter(employee_id=id).update(**content)
        salesperson = Salesperson.objects.get(employee_id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_or_create_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "PUT"])
def api_delete_or_update_customer(request, id):
    if request.method == "DELETE":
        customer = get_object_or_404(Customer, id=id)
        count, _ = customer.delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=id).update(**content)
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_or_create_sale(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson_id = content["salesperson"]
            customer_id = content["customer"]
            automobile_vin = content["automobile"]

            vin = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = vin
            salesperson = Salesperson.objects.get(employee_id=salesperson_id)
            content["salesperson"] = salesperson
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer

            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except (
            AutomobileVO.DoesNotExist,
            Salesperson.DoesNotExist,
            Customer.DoesNotExist,
        ):
            response = JsonResponse(
                {"message": "One of your objects does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["DELETE"])
def api_delete_sale(request, id):
    if request.method == "DELETE":
        sale = get_object_or_404(Sale, id=id)
        count, _ = sale.delete()
        return JsonResponse({"deleted": count > 0})
