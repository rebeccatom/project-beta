from django.shortcuts import render
from .models import Technician, AutomobileVO, Appointment
from django.shortcuts import render, get_object_or_404
import json
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .encoders import AutomobileVOEncoder, TechnicianListEncoder, TechnicianDetailEncoder, AppointmentListEncoder, AppointmentDetailEncoder


@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def show_technician(request, id):
    if request.method == "DELETE":
        technician = get_object_or_404(Technician, employee_id=id)
        count, _ = technician.delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
            safe=False,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.get(
                employee_id=content["technician"])
            content["technician"] = technician

            automobile_vin = content["vin"]
            vin = AutomobileVO.objects.get(vin=automobile_vin)
            content["vin"] = vin
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                {"appointment": appointment},
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except (AutomobileVO.DoesNotExist, Technician.DoesNotExist):
            response = JsonResponse(
                {"message": "Your automobile or technician does not exist"}
            )
            response.status_code = 404
            return response


@require_http_methods(["DELETE"])
def show_appointment(request, id):
    if request.method == "DELETE":
        appointment = get_object_or_404(Appointment, id=id)
        count, _ = appointment.delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["PUT"])
def cancel_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.status = "CANCELED"
    appointment.save()
    return JsonResponse(
        appointment,
        encoder=AppointmentDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def finish_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.status = "FINISHED"
    appointment.save()

    return JsonResponse(
        appointment,
        encoder=AppointmentDetailEncoder,
        safe=False,
    )
