from django.db import models


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.PositiveIntegerField()


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    vin = models.ForeignKey(
        AutomobileVO, related_name="automobile", on_delete=models.CASCADE)
    customer = models.CharField(max_length=50)
    technician = models.ForeignKey(
        Technician, related_name="appointments", on_delete=models.CASCADE)
    status = models.CharField(max_length=10, null=True)
    vip = models.CharField(max_length=5, default="No")
