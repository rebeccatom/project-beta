import { Link } from "react-router-dom";
import React, { useEffect, useState } from "react";

function TechnicianList() {
  const [technicians, setTechnicians] = useState([]);

  async function onDelete(event, technician) {
    event.preventDefault();
    const technicianUrl = `http://localhost:8080/api/technicians/${technician}/`;
    const fetchConfig = {
      method: "delete",
    };
    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
      fetchData();
    }
  }

  const fetchData = async () => {
    const technicianUrl = "http://localhost:8080/api/technicians/";
    const response = await fetch(technicianUrl);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <table className="table table-striped table-hover">
        <thead>
          <tr>
            <th className="text-center">First Name</th>
            <th className="text-center">Last Name</th>
            <th className="text-center">Employee ID</th>
          </tr>
        </thead>
        <tbody>
          {technicians.map((technician) => {
            return (
              <tr key={technician.id}>
                <td className="text-center">{technician.first_name}</td>
                <td className="text-center">{technician.last_name}</td>
                <td className="text-center">{technician.employee_id}</td>
                <td align="right">
                  <button
                    onClick={(event) => onDelete(event, technician.employee_id)}
                    className="btn btn-danger"
                  >
                    REMOVE TECHNICIAN
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link
          to="/technicians/new"
          className="btn btn-primary btn-lg px-4 gap-3"
        >
          Add a Technician
        </Link>
      </div>
    </>
  );
}

export default TechnicianList;
