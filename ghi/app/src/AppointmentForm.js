import React, { useState, useEffect } from "react";

export default function AppointmentForm() {
  const [automobileVins, setAutomobileVins] = useState([]);
  const [technicians, setTechnicians] = useState([]);
  const [automobileVin, setAutomobileVin] = useState("");
  const [customer, setCustomer] = useState("");
  const [dateTime, setDateTime] = useState("");
  const [technician, setTechnician] = useState("");
  const [reason, setReason] = useState("");

  const handleAutomobileVinChange = (event) => {
    const value = event.target.value;
    setAutomobileVin(value);
  };

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };

  const handleDateTimeChange = (event) => {
    const value = event.target.value;
    setDateTime(value);
  };

  const handleTechnicianChange = (event) => {
    const value = event.target.value;
    setTechnician(value);
  };

  const handleReasonChange = (event) => {
    const value = event.target.value;
    setReason(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};

    data.vin = automobileVin;
    data.customer = customer;
    data.date_time = dateTime;
    data.technician = technician;
    data.reason = reason;
    data.status = "SCHEDULED";

    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      setAutomobileVin("");
      setCustomer("");
      setDateTime("");
      setTechnician("");
      setReason("");
    }
  };

  const fetchData = async () => {
    const automobileVinsUrl = "http://localhost:8100/api/automobiles/";
    const techniciansUrl = "http://localhost:8080/api/technicians/";

    const automobileVinsResponse = await fetch(automobileVinsUrl);
    const techniciansResponse = await fetch(techniciansUrl);

    if (automobileVinsResponse.ok && techniciansResponse.ok) {
      const automobileVinsData = await automobileVinsResponse.json();
      const techniciansData = await techniciansResponse.json();
      setAutomobileVins(automobileVinsData.autos);
      setTechnicians(techniciansData.technicians);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Schedule an appointment</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
              <select
                value={automobileVin}
                onChange={handleAutomobileVinChange}
                required
                id="vin"
                name="vin"
                className="form-select"
              >
                <option value="">Choose an automobile VIN</option>
                {automobileVins.map((auto) => {
                  return (
                    <option key={auto.vin} value={auto.vin}>
                      {auto.vin}
                    </option>
                  );
                })}
              </select>
              <label htmlFor="automobile_vin">Automobile VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={customer}
                onChange={handleCustomerChange}
                placeholder="Customer"
                required
                type="text"
                name="customer"
                id="customer"
                className="form-control"
              />
              <label htmlFor="customer">Customer Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={dateTime}
                onChange={handleDateTimeChange}
                placeholder="Date and Time"
                required
                type="datetime-local"
                name="date_time"
                id="date_time"
                className="form-control"
              />
              <label htmlFor="date_time">Date and Time</label>
            </div>
            <div className="form-floating mb-3">
              <select
                value={technician}
                onChange={handleTechnicianChange}
                required
                id="technician"
                name="technician"
                className="form-select"
              >
                <option value="">Choose a Technician</option>
                {technicians.map((technician) => {
                  return (
                    <option
                      key={technician.employee_id}
                      value={technician.employee_id}
                    >
                      {technician.first_name + " " + technician.last_name}
                    </option>
                  );
                })}
              </select>
              <label htmlFor="technician">Technician</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={reason}
                onChange={handleReasonChange}
                placeholder="Reason"
                required
                type="text"
                name="reason"
                id="reason"
                className="form-control"
              />
              <label htmlFor="reason">Reason</label>
            </div>
            <button className="btn btn-primary">Schedule</button>
          </form>
        </div>
      </div>
    </div>
  );
}
