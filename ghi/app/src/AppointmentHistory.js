import { Link } from "react-router-dom";
import React, { useState, useEffect } from "react";

function AppointmentHistory(props) {
  const [searchedVin, setSearchedVin] = useState("");
  const [appointments, setAppointments] = useState([]);
  const [vipVins, setVipVins] = useState([]);
  const fetchData = async () => {
    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const response = await fetch(appointmentUrl);
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSearchedVinChange = (event) => {
    const value = event.target.value;
    setSearchedVin(value);
  };

  const handleSearch = async (event) => {
    event.preventDefault();

    const searchVinUrl = "http://localhost:8080/api/appointments/";
    const response = await fetch(searchVinUrl);
    if (response.ok) {
      const searchVinData = await response.json();
      const filteredAppointments = searchVinData.appointments.filter(
        (appointment) => {
          return appointment.vin.vin === searchedVin;
        }
      );
      setAppointments(filteredAppointments);
    }
  };

  async function fetchVIPS() {
    const vipUrl = "http://localhost:8100/api/automobiles/";
    const response = await fetch(vipUrl);
    if (response.ok) {
      const vipData = await response.json();
      setVipVins(vipData.autos.map((auto) => auto.vin));
    }
  }

  useEffect(() => {
    fetchVIPS();
  }, []);

  function vipVIN(vin) {
    return vipVins.includes(vin) ? "Yes" : "No";
  }

  function formatDate(datetime) {
    const date = Date(datetime);
    return date.toLocaleString();
  }

  if (appointments === undefined) {
    return null;
  }

  return (
    <>
      <h1>Service History</h1>
      <form onSubmit={handleSearch}>
        <div>
          <input
            type="text"
            onChange={handleSearchedVinChange}
            placeholder="Search by VIN"
            name="search"
            id="search"
          />
        </div>
        <button>Search</button>
      </form>
      <table className="table table-striped table-hover">
        <thead>
          <tr>
            <th className="text-center">Date and Time</th>
            <th className="text-center">Reason</th>
            <th className="text-center">Is VIP?</th>
            <th className="text-center">VIN</th>
            <th className="text-center">Customer</th>
            <th className="text-center">Technician</th>
            <th className="text-center">Status</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => {
            return (
              <tr key={appointment.id}>
                <td className="text-center">
                  {formatDate(appointment.date_time)}
                </td>
                <td className="text-center">{appointment.reason}</td>
                <td className="text-center">{vipVIN(appointment.vin.vin)}</td>
                <td className="text-center">{appointment.vin.vin}</td>
                <td className="text-center">{appointment.customer}</td>
                <td className="text-center">
                  {appointment.technician.first_name}
                </td>
                <td className="text-center">{appointment.status}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link
          to="/appointments/new"
          className="btn btn-primary btn-lg px-4 gap-3"
        >
          Schedule an appointment!
        </Link>
      </div>
    </>
  );
}

export default AppointmentHistory;
