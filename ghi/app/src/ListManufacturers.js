import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function ListManufacturers() {
  const [manufacturers, setManufacturers] = useState([]);

  async function onDelete(event, manufacturer) {
		event.preventDefault();
		const manufacturerUrl = `http://localhost:8100/api/manufacturers/${manufacturer}/`;
		const fetchConfig = {
			method: "delete",
		};
		const response = await fetch(manufacturerUrl, fetchConfig);
		if (response.ok) {
			fetchData();
		}
  }
  
  const fetchData = async () => {
    const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(manufacturerUrl);
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  if (manufacturers === undefined) {
    return null;
  }

  return (
    <>
      <h1>Manufacturers</h1>
      <table className="table table-striped table-hover">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map((manufacturer) => {
            return (
							<tr key={manufacturer.id}>
                <td>{manufacturer.name}</td>
                <td align="right">
                  <button
                    onClick={(event) => onDelete(event, manufacturer.id)}
                    className="btn btn-danger"
                  >
                    DELETE
                  </button>
                </td>
							</tr>
						);
          })}
        </tbody>
      </table>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link
          to="/manufacturers/new"
          className="btn btn-primary btn-lg px-4 gap-3"
        >
          Add a New Manufacturer
        </Link>
      </div>
    </>
  );
}

export default ListManufacturers;
