import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function ListVehicleModels() {
  const [models, setModels] = useState([]);

  async function onDelete(event, model) {
    event.preventDefault();
    const modelUrl = `http://localhost:8100/api/models/${model}/`;
    const fetchConfig = {
      method: "delete",
    };
    const response = await fetch(modelUrl, fetchConfig);
    if (response.ok) {
      fetchData();
    }
  }

	const fetchData = async () => {
		const modelUrl = "http://localhost:8100/api/models/";
		const response = await fetch(modelUrl);
		if (response.ok) {
			const data = await response.json();
			setModels(data.models);
		}
	};

	useEffect(() => {
		fetchData();
  }, []);

  if (models === undefined) {
    return null;
  }

	return (
		<>
			<table className="table table-striped table-hover">
				<thead>
					<tr>
						<th>Name</th>
						<th>Manufacturer</th>
						<th>Picture</th>
					</tr>
				</thead>
				<tbody>
					{models.map((model) => {
						return (
							<tr key={model.id}>
								<td>{model.name}</td>
								<td>{model.manufacturer.name}</td>
								<td>
									<img
										className="card img-fluid"
										width="200"
										height="175"
										src={model.picture_url}
										alt=""
									></img>
								</td>
								<td align="right">
									<button
										onClick={(event) => onDelete(event, model.id)}
										className="btn btn-danger"
									>
										DELETE
									</button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link to="/models/new" className="btn btn-primary btn-lg px-4 gap-3">
          Add a New Vehicle Model
        </Link>
      </div>
		</>
	);
}

export default ListVehicleModels;
