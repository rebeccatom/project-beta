import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function ListAutomobiles() {
  const [autos, setAutos] = useState([]);
  const fetchData = async () => {
    const autosUrl = "http://localhost:8100/api/automobiles/";
    const response = await fetch(autosUrl);
    if (response.ok) {
      const data = await response.json();
      setAutos(data.autos);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);
  async function onDelete(event, auto) {
    event.preventDefault();
    const automobileUrl = `http://localhost:8100/api/automobiles/${auto}/`;
    const fetchConfig = {
      method: "delete",
    };
    const response = await fetch(automobileUrl, fetchConfig);
    if (response.ok) {
      fetchData();
    }
  }

  if (autos === undefined) {
    return null;
  }

	return (
		<>
			<table className="table table-striped table-hover">
				<thead>
					<tr>
						<th>VIN</th>
						<th>Color</th>
						<th>Year</th>
						<th>Model</th>
						<th>Manufacturer</th>
						<th>Sold</th>
					</tr>
				</thead>
				<tbody>
					{autos.map((auto) => {
						if (auto.sold === false) {
							return (
								<tr key={auto.vin}>
									<td>{auto.vin}</td>
									<td>{auto.color}</td>
									<td>{auto.year}</td>
									<td>{auto.model.name}</td>
									<td>{auto.model.manufacturer.name}</td>
									<td>No</td>
									<td>
										<button
											onClick={(event) => onDelete(event, auto.vin)}
											className="btn btn-danger"
										>
											DELETE
										</button>
									</td>
								</tr>
							);
						} else {
							return (
								<tr key={auto.id}>
									<td>{auto.vin}</td>
									<td>{auto.color}</td>
									<td>{auto.year}</td>
									<td>{auto.model.name}</td>
									<td>{auto.model.manufacturer.name}</td>
									<td>Yes</td>
									<td align="right">
										<button
											onClick={(event) => onDelete(event, auto.vin)}
											className="btn btn-danger"
										>
											DELETE
										</button>
									</td>
								</tr>
							);
						}
					})}
				</tbody>
			</table>
			<div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
				<Link to="/autos/new" className="btn btn-primary btn-lg px-4 gap-3">
					Add an automobile
				</Link>
			</div>
		</>
	);
}

export default ListAutomobiles;
