import React, { useState, useEffect } from "react";

export default function CreateVehicleModel() {
	const [manufacturers, setManufacturers] = useState([]);

	const [modelName, setModelName] = useState("");
	const handleModelNameChange = (event) => {
		const value = event.target.value;
		setModelName(value);
	};

	const [picture, setPicture] = useState("");
	const handlePictureChange = (event) => {
		const value = event.target.value;
		setPicture(value);
	};

	const [manufacturer, setManufacturer] = useState("");
	const handleManufacturerChange = (event) => {
		const value = event.target.value;
		setManufacturer(value);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {};

		data.name = modelName;
		data.picture_url = picture;
		data.manufacturer_id = manufacturer;

		const modelsUrl = "http://localhost:8100/api/models/";
		const fetchConfig = {
			method: "post",
			body: JSON.stringify(data),
			headers: {
				"Content-Type": "application/json",
			},
		};
		const response = await fetch(modelsUrl, fetchConfig);
		if (response.ok) {
			setModelName("");
			setPicture("");
			setManufacturer("");
		}
	};
	const fetchData = async () => {
		const manufacturersUrl = "http://localhost:8100/api/manufacturers/";

		const manufacturersResponse = await fetch(manufacturersUrl);

		if (manufacturersResponse.ok) {
			const manufacturersData = await manufacturersResponse.json();
			setManufacturers(manufacturersData.manufacturers);
		}
	};
	useEffect(() => {
		fetchData();
	}, []);
	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Create a vehicle model</h1>
					<form onSubmit={handleSubmit} id="create-model-form">
						<div className="form-floating mb-3">
							<input
								value={modelName}
								onChange={handleModelNameChange}
								placeholder="Model Name"
								required
								type="text"
								name="model_name"
								id="model_name"
								className="form-control"
							/>
							<label htmlFor="model_name">Model Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={picture}
								onChange={handlePictureChange}
								placeholder="Picture"
								required
								type="url"
								name="picture"
								id="picture"
								className="form-control"
							/>
							<label htmlFor="picture">Picture URL</label>
						</div>
						<div className="mb-3">
							<select
								value={manufacturer}
								onChange={handleManufacturerChange}
								required
								id="manufacturer"
								name="manufacturer"
								className="form-select"
							>
								<option value="">Choose a manufacturer</option>
								{manufacturers.map((manufacturer) => {
									return (
										<option key={manufacturer.id} value={manufacturer.id}>
											{manufacturer.name}
										</option>
									);
								})}
							</select>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}
