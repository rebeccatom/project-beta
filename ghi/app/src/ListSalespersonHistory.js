import React, { useState, useEffect } from "react";

function ListSalespersonHistory() {
	const [sales, setSales] = useState([]);

	const [salespeople, setSalespeople] = useState([]);

	const [salespersonSales, setSalespersonSales] = useState([]);

	const [salesperson, setSalesperson] = useState("");
	const handleSalespersonChange = (event) => {
		const value = event.target.value;
		setSalesperson(value);
	};

	const fetchData = async () => {
		const salespeopleUrl = "http://localhost:8090/api/salespeople/";
		const salesUrl = "http://localhost:8090/api/sales/";

		const salespeopleResponse = await fetch(salespeopleUrl);
		const salesResponse = await fetch(salesUrl);

		if (salespeopleResponse.ok && salesResponse) {
			const salespeopleData = await salespeopleResponse.json();
			const salesData = await salesResponse.json()

			setSalespeople(salespeopleData.salespeople);
			setSales(salesData.sales)

		}
	};
	useEffect(() => {
		fetchData();
	}, []);

	useEffect(() => {
		const salespersonSales = sales.filter((sale) => {
			return String(sale.salesperson.employee_id) === String(salesperson);
		});
		setSalespersonSales(salespersonSales);
	}, [sales, salesperson]);

	if (sales === undefined || salespeople === undefined) {
		return null;
	}

	return (
		<>
			<h1>Salesperson History</h1>
			<div className="mb-3">
				<select
					value={salesperson}
					onChange={handleSalespersonChange}
					required
					id="salesperson"
					name="salesperson"
					className="form-select"
				>
					<option value="">Choose a salesperson</option>
					{salespeople.map((salesperson) => {
						return (
							<option
								key={salesperson.employee_id}
								value={salesperson.employee_id}
							>
								{salesperson.first_name + " " + salesperson.last_name}
							</option>
						);
					})}
				</select>
			</div>
			<table className="table table-striped table-hover">
				<thead>
					<tr>
						<th className="text-center">Salesperson Name</th>
						<th className="text-center">Customer</th>
						<th className="text-center">VIN</th>
						<th className="text-center">Price</th>
					</tr>
				</thead>
				<tbody>
					{salespersonSales.map((sale) => {
						return (
							<tr key={sale.id}>
								<td className="text-center">
									{sale.salesperson.first_name +
										" " +
										sale.salesperson.last_name}
								</td>
								<td className="text-center">
									{sale.customer.first_name + " " + sale.customer.last_name}
								</td>
								<td className="text-center">{sale.automobile.vin}</td>
								<td className="text-center">${sale.price}.00</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</>
	);
}

export default ListSalespersonHistory;
