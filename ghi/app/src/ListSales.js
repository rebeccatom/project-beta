import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

function ListSales() {
	const [sales, setSales] = useState([]);

	async function onDelete(event, sale, vin) {

		event.preventDefault();
		const saleUrl = `http://localhost:8090/api/sales/${sale}/`;
		const saleFetchConfig = {
			method: "delete",
		};

		const autoData = {};

		autoData.sold = false;

		const autoUrl = `http://localhost:8100/api/automobiles/${vin}/`;
		const autoFetchConfig = {
			method: "put",
			body: JSON.stringify(autoData),
			headers: {
				"Content-Type": "application/json",
			},
		};
		const response = await fetch(saleUrl, saleFetchConfig);
		const autoResponse = await fetch(autoUrl, autoFetchConfig);
		if (response.ok && autoResponse.ok) {
			fetchData();
		}
	}
	const fetchData = async () => {
		const salesUrl = "http://localhost:8090/api/sales/";
		const response = await fetch(salesUrl);
		if (response.ok) {
			const data = await response.json();
			setSales(data.sales);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);
	if (sales === undefined) {
		return null;
	}

	return (
		<>
			<table className="table table-striped table-hover">
				<thead>
					<tr>
						<th className="text-center">Salesperson Name</th>
						<th className="text-center">Salesperson Employee ID</th>
						<th className="text-center">Customer</th>
						<th className="text-center">VIN</th>
						<th className="text-center">Price</th>
					</tr>
				</thead>
				<tbody>
					{sales.map((sale) => {
						return (
							<tr key={sale.id}>
								<td className="text-center">
									{sale.salesperson.first_name +
										" " +
										sale.salesperson.last_name}
								</td>
								<td className="text-center">{sale.salesperson.employee_id}</td>
								<td className="text-center">
									{sale.customer.first_name + " " + sale.customer.last_name}
								</td>
								<td className="text-center">{sale.automobile.vin}</td>
								<td className="text-center">${sale.price}.00</td>
								<td align="right">
									<button
										onClick={(event) =>
											onDelete(event, sale.id, sale.automobile.vin)
										}
										className="btn btn-danger"
									>
										DELETE
									</button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
			<div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
				<Link to="/sales/new" className="btn btn-primary btn-lg px-4 gap-3">
					Add a Sale
				</Link>
			</div>
		</>
	);
}

export default ListSales;
