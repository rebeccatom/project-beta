import React, { useEffect, useState } from 'react'
import { Link } from "react-router-dom";

function ListSalespeople() {
	const [salespeople, setSalespeople] = useState([]);

	async function onDelete(event, salesperson) {
		event.preventDefault();
		const salespersonUrl = `http://localhost:8090/api/salespeople/${salesperson}/`;
		const fetchConfig = {
			method: "delete",
		};
		const response = await fetch(salespersonUrl, fetchConfig);
		if (response.ok) {
			fetchData();
		}
	}
	const fetchData = async () => {
		const salespeopleUrl = "http://localhost:8090/api/salespeople/";
		const response = await fetch(salespeopleUrl);
		if (response.ok) {
			const data = await response.json();
			setSalespeople(data.salespeople);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

	if (salespeople === undefined) {
		return null;
	}

	return (
		<>
			<table className="table table-striped table-hover">
				<thead>
					<tr>
						<th>Employee ID</th>
						<th>First Name</th>
						<th>Last Name</th>
					</tr>
				</thead>
				<tbody>
					{salespeople.map((salesperson) => {
						return (
							<tr key={salesperson.employee_id}>
								<td>{salesperson.employee_id}</td>
								<td>{salesperson.first_name}</td>
								<td>{salesperson.last_name}</td>
								<td align="right">
									<button
										onClick={(event) =>
											onDelete(event, salesperson.employee_id)
										}
										className="btn btn-danger"
									>
										DELETE
									</button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
			<div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
				<Link
					to="/salespeople/new"
					className="btn btn-primary btn-lg px-4 gap-3"
				>
					Add an Employee
				</Link>
			</div>
		</>
	);
}

export default ListSalespeople;
