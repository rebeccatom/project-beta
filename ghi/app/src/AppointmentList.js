import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function AppointmentList() {
  const [vipVins, setVipVins] = useState([]);
  const [appointments, setAppointment] = useState([]);

  async function fetchVIPS() {
    const vipUrl = "http://localhost:8100/api/automobiles/";
    const response = await fetch(vipUrl);
    if (response.ok) {
      const vipData = await response.json();
      setVipVins(vipData.autos.map((auto) => auto.vin));
    }
  }

  useEffect(() => {
    fetchVIPS();
  }, []);

  function vipVIN(vin) {
    return vipVins.includes(vin) ? "Yes" : "No";
  }

  async function onCancel(event, id) {
    const appointmentUrl = `http://localhost:8080/api/appointments/${id}/cancel/`;
    const fetchConfig = {
      method: "put",
    };
    const _ = await fetch(appointmentUrl, fetchConfig);

    setAppointment(appointments.filter((a) => a.id !== id));
  }

  async function onFinish(event, id) {
    const appointmentUrl = `http://localhost:8080/api/appointments/${id}/finish/`;
    const fetchConfig = {
      method: "put",
    };
    const _ = await fetch(appointmentUrl, fetchConfig);

    setAppointment(appointments.filter((a) => a.id !== id));
  }

  const fetchData = async () => {
    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const response = await fetch(appointmentUrl);
    if (response.ok) {
      const data = await response.json();
      setAppointment(data.appointments);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  function formatDate(datetime) {
    const date = Date(datetime);
    return date.toLocaleString();
  }
  return (
    <>
      <table className="table table-striped table-hover">
        <thead>
          <tr>
            <th className="text-center">Date and Time</th>
            <th className="text-center">Reason</th>
            <th className="text-center">VIN</th>
            <th className="text-center">Technician</th>
            <th className="text-center">Status</th>
            <th className="text-center">Is VIP?</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => {
            if (String(appointment.status) === "SCHEDULED") {
              return (
                <tr key={appointment.id}>
                  <td className="text-center">
                    {formatDate(appointment.date_time)}
                  </td>
                  <td className="text-center">{appointment.reason}</td>
                  <td className="text-center">{appointment.vin.vin}</td>
                  <td className="text-center">
                    {appointment.technician.first_name +
                      " " +
                      appointment.technician.last_name}
                  </td>
                  <td className="text-center">{appointment.status}</td>
                  <td className="text-center">{vipVIN(appointment.vin.vin)}</td>
                  <td>
                    <button
                      onClick={(event) => onCancel(event, appointment.id)}
                      className="btn btn-danger"
                    >
                      CANCEL
                    </button>
                  </td>
                  <td>
                    <button
                      onClick={(event) => onFinish(event, appointment.id)}
                      className="btn btn-success"
                    >
                      FINISH
                    </button>
                  </td>
                </tr>
              );
            }
          })}
        </tbody>
      </table>
      <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
        <Link
          to="/appointments/new"
          className="btn btn-primary btn-lg px-4 gap-3"
        >
          Schedule a New Appointment
        </Link>
      </div>
    </>
  );
}

export default AppointmentList;
